// two app.run

public void Configure(IApplicationBuilder app)
{

	// first app.run
	// this will terminate the pipeline

	app.Run(async=>

		await context.Response.WriteAsync("Hello World");
	);

	// second app.run will not run

	app.Run(async=>
		await context.Response.WriteAsynce("Hello World, Again!");
	);
}