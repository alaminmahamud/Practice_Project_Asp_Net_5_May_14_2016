app.Use(async(HttpContext httpContext, Func<Task> next)=>{
    var sw = Stopwatch.StartNew();
    httpContext.Response.OnSendingHeaders((state)=>{
        sw.Stop();
        httpContext.Response.Headers.Add("X-Response-Time", new string[]{sw.ElapsedMilliseconds.ToString() + "ms"});
    });
});