public void Configure(IApplicationBuilder app)
{
    app.UseMvc();

    app.MapWhen(async (context) =>
        {
            return context.Request.Query.ContainsKey("q");
        }, HandleQuery
    );

    app.Run(async context =>
        {
            await context.Response.WriteAsync("Returned From Config");
        }
    );
     

}

private void HandleQuery(IApplicationBuilder app)
{
    app.Run
    (async context =>
       { await context.Response.WriteAsync("Returned From HandleQuery");
       }
    );
}