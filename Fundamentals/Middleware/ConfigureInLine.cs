public void ConfigureLogInline
(
IApplicationBuilder app, ILoggerFactory loggerFactory
)
{
	loggerFactory.AddConsole(minLevel: Loglevel.Information);
	var logger = loggerFactory.CreateLogger(_environment);

	app.Use(async (context,next)=>
	{
		logger.LogInformation("Starting to Process Reqeust");
		await next.Invoke();
		logger.LogInformation("Finished Handling Request");
	});

	app.Run(async context =>
		await context.Response.WriteAsync("Hello World + _environment");
	);
}