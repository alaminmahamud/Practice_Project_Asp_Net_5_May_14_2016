app.Use
(
    (HttpContext context, Func<Task> next)=>
    {
        Console.WriteLine("Bla Bla");
        var task = new Task(() => Console.WriteLine("More Bla Bla Stuffs"));
        task.Start();
        return task;
    }   
);