using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1
{
    public class ResponseTimerMiddleware
    {
        private readonly RequestDelegate next;

        public ResponseTimerMiddleware(RequestDelegate _next)
        {
            next = _next;
        }

        public Task Invoke(HttpContext httpContext)
        {
            var sw = Stopwatch.StartNew();

            httpContext.Response.OnSendingHeaders((state)=> {
                sw.Stop();
                await httpContext.Response.Headers.Add("X-Response-Time ", new string[] { sw.ElapsedMilliseconds.ToString(); } + " ms" );
            },null);

            return next(httpContext);
        }
    }
}
