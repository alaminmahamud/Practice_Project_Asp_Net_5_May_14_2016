private static void HandleMapTest(IApplicationBuilder app)
{
	app.Run(

		async context =>
		{
			await context.Response.WriteAsync("Map Test Successful");
		}	
	);
}

public void ConfigMapping(IApplicationBuilder app)
{
	app.Map("/maptest", HandleMapTest);
}