public void ConfigureLogMiddleware(IApplicationBuilder app, ILoggerFactory loggerFactory)
{
    loggerFactory.AddConsole(minLevel: loglevel.Information);
    
    // How UseRequestLogger can be extension method
    // how do you determine that
    
    // Answer : Using Statement
    
    app.UseRequestLogger();
    
    app.Run(asynce (context) =>{
        
        await context.Response.WriteAsync("Hello World!");
        
    });
}