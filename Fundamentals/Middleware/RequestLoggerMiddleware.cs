using Microsoft.AspNet.Builder;
using Microsfot.AspNet.Http;
using Microsoft.Framework.Logging;
using System.Threading.Tasks;


namespace MiddlewareSample
{
    pubilc class RequestLoggerMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;
        
        public RequestLoggerMiddleware(RequestDelegate next, ILogger loggerFactory)
        {
            _next = next;
            _logger = loggerFactory;
        }
        
        public async Task Invoke(HttpContext context)
        {
            _logger.LogInformation("Handling Request : " + context.Request.Path);
            await _next.Invoke(context);
            _logger.LogInformation("Finished Handling Request ");
        }
    }
}
