public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
{
    ...
    
    // Using Directory browsing
    app.UseDirectoryBrowser();
    
    // Using FileExtensionContentTypeProvider for mapping '.myapp' extension to 'application/x-msdownload'
    var provider = new FileExtensionContentTypeProvider();
    provider.Mappings.Add(".myapp", "application/x-msdownload");
    
    // Adding Static File Support in asp.net
    // Serve Static Files
    app.UseStaticFiles(new StaticFileOptions{ContentProvider = provider});
}