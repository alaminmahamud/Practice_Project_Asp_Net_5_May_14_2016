public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
{
    app.UseDirectoryBrowser(new DirectoryBrowserOptions()
    {
       FileProvider = new PhysicalFileProvider(@"D:\Source\WebApplication1\src\WebApplication1\MyStaticFiles"),
       RequestPath = new PathString("/StaticFiles")    
    });   
}