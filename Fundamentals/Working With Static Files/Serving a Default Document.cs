public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
{
    ...
    
    /// Serving Default Files
    app.UseDefaultFiles();
    app.UseStaticFiles();
    
    ...
    
}   