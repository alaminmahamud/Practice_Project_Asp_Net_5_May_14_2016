public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory log)
{
    ...
    /// Add Static files to the Request Pipeline
    app.UseStaticFiles();
    ...
}